<?php 
	//-------------------------Function for home.php-------------------------------------- 
		//||||||| create a file
		function createFile($fileName){
			$myfile = fopen($fileName, "w");
			fclose($myfile);
		}
		//||||||| write in a file
		function writeFile($fileName,$value){
			$myfile = fopen($fileName, "w") or die("Unable to open file!");
			fwrite($myfile, $value);
			fclose($myfile);
		}
		//||||||| Delete a file
		function deleteFile($fileName){
			$myFile = $fileName;
			unlink($myFile) or die("Couldn't delete file");
		}
		//||||||| Read a file | return the value of the file
		function readMyFile($fileName){
			$myfile = fopen($fileName, "r") or die("Unable to open file!");
			$value = fread($myfile,filesize($fileName));
			fclose($myfile);
			return $value;
		}
		//||||||| Upload file
		function uploadFile($path){
			if(!empty($_FILES['uploaded_file'])){
			    $path = $path . basename( $_FILES['uploaded_file']['name']);
			    
			    if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
			      echo "The file ".  basename( $_FILES['uploaded_file']['name']). 
			      " has been uploaded";
			      header( "refresh:2;url=Home.php" );
			    } else{
			        echo "There was an error uploading the file, please try again!";
			    }
			}
		}

		//||||||| Download any document from server function
		function downloadFile($fileName){
			echo "<a href='".$fileName."'>Download this</a>";
		}

		//||||||| Create a folder or directory for user with mode (rules) 
		function createFolder($folderName){
			if (!is_dir($folderName)) {
    			mkdir($folderName);         
				echo "'".$folderName ."' folder is created!";
			}
			else{
				echo "Folder exist already!";
			}
		}

		//||||||| Delete a folder
		function deleteFolder($folderName){
		    if(is_dir($folderName)){
		        $files = glob($folderName . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

		        foreach($files as $file){
		            deleteFolder($file);      
		        }
		        if(is_dir($folderName))
		        	rmdir($folderName);
		    echo "'".$folderName ."' folder is deleted!<br>";
		    } elseif(is_file($folderName)) {
		        unlink($folderName);  
		    }
		}

	//-------------------------Functions for login.php-------------------------------------- 
	
		////////// check username if exist in the file
	function checkUsernameLogin($file,$usernameToCheck){
		$handle = fopen($file, "r");
		$value = "";
		if ($handle) {
		    while (($line = fgets($handle)) !== false) {
		        // process the line read.
		        $word = explode(" ", $line);
		        if($usernameToCheck == $word[0]){
		        	$value = $line;
		        	return $value;
		        }
	    	}
	    	fclose($handle);
		}
		return $value; //if value return "" (blank) the username is wrong - if value return something username exist
	}

		///////////// password dehash to compare with what the user input
	function passwordDehash($username, $password, $passwordHashed){
		$hashedU = hash('sha256', $username);
		$check = $password.$hashedU;
		if (password_verify($check, $passwordHashed)) {
	    	return 1; // return 1 if it is valid
		} else {
		    return 2; // return 2 if it is not valid
		}
	}

		////////// last function before letting the user log in
	function allowUserConnect($fileName, $password, $account){
		if($account != ""){
			$Account = explode(" ", $account);
			$dehash = passwordDehash($Account[0], $password, $Account[1]); // return 1 if ok and 2 if not ok
		}
		else
			$dehash = 2;
		return $dehash; // 1 if ok and 2 if not ok
	}
	
	//-------------------------Functions for Sign up.php-------------------------------------- 
	

	// ||||||| function to check if the username exist already
	function checkUsernameSignUp($file,$usernameToCheck){
		$handle = @fopen($file, "r");
		$i = 0;
		$ok = " ";
		// check the username
		if ($handle) {
		    while (!feof($handle)) {
		        $buffer = fgets($handle); // fgets: Function to get a line in a file
		        $exploded_data = explode(" ",$buffer); // explode take word per word and put it into an array which is $exploded_data
		        if ($usernameToCheck == $exploded_data[0]){ //check if the username exist already
		        	$ok = $exploded_data;
		        	echo "Username already exist, please choose another username!<br>";
		 			break;
		        }
		    	$i++; // number of account = $i-1
		    }
		    fclose($handle);
		}
		return $ok; // if ok equal " " => username does not exist
	}



	// ||||||| function: check if password is secure enough
	function checkPasswordSecure($pwd) {
    $errors = "";

    if (strlen($pwd) < 8) {
        $errors = $errors . "Password too short!<br>";
    }

    if (!preg_match("#[0-9]+#", $pwd)) {
        $errors= $errors . "Password must include at least one number!<br>";
    }

    if (!preg_match("#[a-zA-Z]+#", $pwd)) {
        $errors= $errors . "Password must include at least one letter!<br>";
    }     

    	return $errors;
	}	


	// ||||||| function: check if the password are equal
	function checkPasswordSignUp($pwd1, $pwd2){
		if($pwd1 == $pwd2){
			return "";
		}
		else{
			return "Your password is not equal retype again!";
		}
	}

	// hashing the password before saving into the file
	function hashPwd($password, $username){
		$salt = hash("sha256", $username);
		$passwordToHash = $password.$salt;
		$value = password_hash($passwordToHash, PASSWORD_BCRYPT);	
		return $value;
	}
?>