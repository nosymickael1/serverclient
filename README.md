DEADLINE : 20 January 23h59

Word for the project summary : https://docs.google.com/document/d/1-FKixZyNWm30YgJBUgNJtPh2atrV5sImtSgEibEwrPY/edit?usp=sharing

Word for the rapport (the one we will give the teacher): https://docs.google.com/document/d/1dQFsooy8c3yilnOuxmeo6514AG3sOlxblGKYypTITBg/edit?usp=sharing


There is LEVEL 1 & LEVEL 2 in the word.
LEVEL 1: Installation of the file system
LEVEL 2: Security related

Our job for now is to FOCUS ONLY on finishing the LEVEL 1 part of the project.

All the file I have created are in the "server & client" directory above. Please download it, and keep a save of your changes in your computer.

DESCRIPTION OF EACH FILE:

- Account.txt:

It contains the username & password of each account created.
We will check this file anytime someone will sign up to see if the username they put exist already.
We will check this file anytime someone will log in to see if the username & password match, if yes then we let them connect if not we do not let them connect.

The order of information in the file:
firstName LastName email username saltFromHashedUsername password+saltHashed  

- Auth.php:

Sign up.php & Login.php are just pages where the user enter their information. Their information will be trated by 
our function in auth.php before we let them connect or create an account.
The job of this file is to just to check the information input by the user.

- Function.php:

To make auth.php a clean file, all of our function are created in function.php, whether it is a function for 
sign up.php, login.php or home.php we created all of them in this file.
Which means we include this file anytime we need a function.

- Home.php:

This is the landing page after the admin or the user login. Functionnality of the file system will be shown here according to the user right.

- Index.php:

The login page is included in index.php to make it clean

- Log out.php:

When a user log out, they are redirected to this link so that we can stop the session.

- Login.php:

It Contains the form for login.

- Sign up.php:

It contains the form for signing up.